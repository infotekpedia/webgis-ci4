 <footer>
     <div class="footer clearfix mb-0 text-muted">
         <div class="float-start">
             <p>2021 &copy; WebGIS CI4 - by <a href="https://www.youtube.com/NasrullahSiddik" target="_blank">ASSHIDDIQ</a></p>
         </div>
         <div class="float-end">
             <p>Crafted with <span class="text-danger"><i class="bi bi-heart"></i></span> Template by <a href="http://ahmadsaugi.com">A. Saugi</a></p>
         </div>
     </div>
 </footer>